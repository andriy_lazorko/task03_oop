package model;

public class Attraction {
    private int time;
    private int price;
    private String nameAttraction;

    public Attraction(int time, int price, String name) {
        this.time = time;
        this.price = price;
        this.nameAttraction = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNameAttraction() {
        return nameAttraction;
    }

    public void setNameAttraction(String nameAttraction) {
        this.nameAttraction = nameAttraction;
    }

    @Override
    public String toString() {
        return "Attraction{" +
                "time=" + time +
                ", price=" + price +
                ", nameAttraction='" + nameAttraction + '\'' +
                '}';
    }
}
