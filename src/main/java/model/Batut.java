package model;

public class Batut extends Attraction {
    private boolean outside = true;

    public Batut(int time, int price, String nameAttraction, boolean outside) {
        super(time, price, nameAttraction);
        this.outside = outside;
    }
}
