package model;

import java.lang.management.GarbageCollectorMXBean;

public class Application {
    public static void main(String[] args) {
        System.out.println("Hello");
        Attraction batutBlack = new Batut(15,250,"Batut Black",true);
        Attraction batutRed = new Batut(15, 240, "Batut Red",true);
        Attraction batutGreen = new Batut(10, 100, "Batut green",false);
        Attraction bigGinderbread = new DrawGingerbread(20, 150, "Draw big gingerbread",true);
        Attraction littleGinderbread = new DrawGingerbread(20,140,"Draw little gingerbread",true);
        Attraction flyBubles = new Bubles(12,50,"Bubles");
        Attraction youngAnimator = new Animators(60,1500,"Sniguronyka");
        Attraction oldAnimator = new Animators(60, 2500,"Santa");

        System.out.println(batutBlack+" put 1 ");
        System.out.println(batutRed+" put 2 ");
        System.out.println(batutGreen+" put 3 ");
        System.out.println(bigGinderbread+" put 4 ");
        System.out.println(littleGinderbread+" put 5 ");
        System.out.println(flyBubles+" put 6 ");
        System.out.println(youngAnimator+" put 7 ");
        System.out.println(oldAnimator+" put 8 ");


    }

}
