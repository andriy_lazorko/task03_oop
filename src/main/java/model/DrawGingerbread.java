package model;

public class DrawGingerbread extends Attraction {
    private boolean house = true;

    public DrawGingerbread(int time, int price, String nameAttraction, boolean house) {
        super(time, price, nameAttraction);
        this.house = house;
    }
}

